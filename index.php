<!DOCTYPE html>
<html lang="en-us">
	<head>
		<title>Responsive</title> 
		<meta charset="UTF-8" > 
		<link rel="stylesheet" type="text/css" href="myStyle.css">
	</head>
	<body>
		<h1>Responsive Web Design Demo</h1>
		<h2>Resize this responsive page!</h2>


		<div class="topnav" id="myTopnav">
	  		<a href="#home">Home</a>
	  		<a href="#news">News</a>
	  		<a href="#contact">Contact</a>
	  		<a href="#about">About</a>
	  		<a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
		</div> 
		
		<div class="city">
			<h2>London</h2>
			<p>London is the capital of England.</p>
			<p>It is the most populous city in the United Kingdom, with a metropolitan area of over 13 million inhabitants.</p>
		</div>

		<div class="city">
			<h2>Paris</h2>
			<p>Paris is the capital of France.</p> 
			<p>The Paris area is one of the largest population centers in Europe, with more than 12 million inhabitants.</p>
		</div>

		<iframe id="myFrame" src='./files/operations.php' name="iframe_1" ></iframe>
		<iframe id="myFrame" src='./files/introduction.php' name="iframe_2" ></iframe>

		<script src="myScript.js">  </script>

	</body>
</html>
